<?php
	function GetCSS(){
		$ret = array();
		$path = "css";
		if(is_dir($path)){
			foreach(scandir($path) as $scandir){
				$relativepath = $path . "/" . $scandir;
				$realpath = realpath($path . DIRECTORY_SEPARATOR . $scandir);
				
				if(
					is_file($realpath) &&
					(pathinfo($realpath, PATHINFO_EXTENSION) == "css")
				){
					array_push($ret, $relativepath);
				}
			}
		}
		
		return array_unique($ret);
	}
	
	function GetJS(){
		$ret = array(
			"js/jquery-3.4.1.min.js",
			"js/jquery-migrate-1.4.1.min.js",
			"js/bootstrap.min.js",
			"js/Base64.js",
			"js/RestAPI.js"
		);
		$path = "js";
		if(is_dir($path)){
			foreach(scandir($path) as $scandir){
				$relativepath = $path . "/" . $scandir;
				$realpath = realpath($path . DIRECTORY_SEPARATOR . $scandir);
				
				if(
					is_file($realpath) &&
					(pathinfo($realpath, PATHINFO_EXTENSION) == "js")
				){
					array_push($ret, $relativepath);
				}
			}
		}
		
		return array_unique($ret);
	}
	
	function GetLayout(){
		$ret = array();
		$path = "html";
		if(is_dir($path)){
			foreach(scandir($path) as $scandir){
				$realpath = realpath($path . DIRECTORY_SEPARATOR . $scandir);
				
				if(
					is_file($realpath) &&
					// (mime_content_type($realpath) == "text/html") &&
					(pathinfo($realpath, PATHINFO_EXTENSION) == "html")
				){
					$ret[pathinfo($scandir, PATHINFO_FILENAME)] = base64_encode(@file_get_contents($realpath));
				}
			}
		}
		
		return $ret;
	}
?>