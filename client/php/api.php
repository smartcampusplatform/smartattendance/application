<?php
	function RestAPI($URL, $Data, $Callback){
		$curl = curl_init();
		// curl_setopt($curl,	CURLOPT_HEADER,			true);
		curl_setopt($curl,	CURLOPT_RETURNTRANSFER,	true);
		curl_setopt($curl,	CURLOPT_SSL_VERIFYPEER,	false);
		curl_setopt($curl,	CURLOPT_CONNECTTIMEOUT,	3);
		curl_setopt($curl,	CURLOPT_TIMEOUT,		300);
		curl_setopt($curl,	CURLOPT_CUSTOMREQUEST,	"POST");
		curl_setopt($curl,	CURLOPT_FOLLOWLOCATION,	true);
		curl_setopt($curl,	CURLOPT_MAXREDIRS,		9);
		
		
		$header = array("Content-Type" => "application/json");
		curl_setopt($curl,	CURLOPT_URL,			$URL);
		
		curl_setopt($curl,	CURLOPT_HTTPHEADER,		$header);
		
		curl_setopt($curl,	CURLOPT_POST,			count($Data));
		curl_setopt($curl,	CURLOPT_POSTFIELDS,		json_encode($Data));
		
		$buffer = curl_exec($curl);
		$info = curl_getinfo($curl);
		$error = curl_errno($curl);
		curl_close($curl);

		if(!$error){
			$ret = true;
			if(is_callable($Callback)){
				$response = json_decode($buffer, true);
				$Callback(($response ? $response : array()));
			}
		}else{
			$ret = false;
		}
		
		return $ret;
	}
	
	function GetToken(){
		$ret = (isset($_SESSION["token"]) ? $_SESSION["token"] : "");
		$previous = $ret;
		
		if(RestAPI(
			AAA_AUTH, 
			array(
				"key" => AAA_KEY,
				"token" => $previous
			), 
			function($Data) use(&$ret){
				$ret = (isset($Data["token"]) ? $Data["token"] : "");
			}
		));
		
		if($ret != $previous){
			$_SESSION["token"] = $ret;
		}
		
		return $ret;
	}
?>