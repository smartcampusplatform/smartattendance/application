var ATTENCANDE_SUPERVISIOR_OFFSET = 0;

function attendance_supervisior_record(Offset = -1){
	API(
		'/attendance/supervisior', 
		{ 'offset': (Offset != -1 ? Offset : ATTENCANDE_SUPERVISIOR_OFFSET) }, 
		function(Result){		
			var 
				pagenation_html = [],
				table_html = [],
				pointer = 0;
			
			for(pointer = 0; pointer <= Result.limit; pointer++){
				pagenation_html.push(
					'<li class="page-item">' +
						'<a class="page-link" href="#" onClick="attendance_supervisior_record(' + pointer + ')">' +
						(pointer+1).toString() +
						'</a>' +
					'</li>'
				);
			}
			
			$('#attendance_supervisior .pagination').html(pagenation_html.join(''));
			
			pointer = (Result.offset * RECORD_LIMIT) + 1;
			Result.record.forEach(function(data){
				table_html.push(
					'<tr>' +
						'<td>' + (pointer++) + '</td>' +
						'<td>' + data.nickname + '</td>' +
						'<td>' + data.onduty + '</td>' +
						'<td>' + data.offduty + '</td>' +
						'<td>' + data.message + '</td>' +
						'<td>' +
							'<button type="button" class="btn btn-success" onClick="attendance_supervisior_decision(' + data.attendance + ', true)">Terima</button>' +
							'<button type="button" class="btn btn-danger" onClick="attendance_supervisior_decision(' + data.attendance + ', false)">Tolak</button>' +
						'</td>' +
					'</tr>'
				);				
			});
			$('#attendance_supervisior .table tbody').html(table_html.join(''));
			
			ATTENCANDE_SUPERVISIOR_OFFSET = Result.offset;
		}
	);
};

function attendance_supervisior_decision(Handle, Value){
	if(confirm('Keputusan: ' + (Value ? 'Diterima' : 'Ditolak') + '?')){
		API(
			'/attendance/correction_supervisior', 
			{ 
				'handle': Handle,
				'decision': Value
			}, 
			function(Result){
				attendance_supervisior_record(ATTENCANDE_SUPERVISIOR_OFFSET);
			}
		);
	}
};