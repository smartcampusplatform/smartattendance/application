var ATTENCANDE_HRD_OFFSET = 0,
	ATTENCANDE_HRD_LIMIT = 0;

function attendance_hrd_record(Offset = -1){
	API(
		'/attendance/hrd', 
		{ 'offset': (Offset != -1 ? Offset : ATTENCANDE_HRD_OFFSET) }, 
		function(Result){		
			var 
				pagenation_html = [],
				table_html = [],
				pointer = 0;
			
			for(pointer = 0; pointer <= Result.limit; pointer++){
				pagenation_html.push(
					'<li class="page-item">' +
						'<a class="page-link" href="#" onClick="attendance_hrd_record(' + pointer + ')">' +
						(pointer+1).toString() +
						'</a>' +
					'</li>'
				);
			}
			
			$('#attendance_hrd .pagination').html(pagenation_html.join(''));
			
			pointer = (Result.offset * RECORD_LIMIT) + 1;
			Result.record.forEach(function(data){
				table_html.push(
					'<tr>' +
						'<td>' + (pointer++) + '</td>' +
						'<td>' + data.nickname + '</td>' +
						'<td>' + data.onduty + '</td>' +
						'<td>' + data.offduty + '</td>' +
						'<td>' + data.message + '</td>' +
						'<td>' +
							'<button type="button" class="btn btn-success" onClick="attendance_hrd_decision(' + data.attendance + ', true)">Terima</button>' +
							'<button type="button" class="btn btn-danger" onClick="attendance_hrd_decision(' + data.attendance + ', false)">Tolak</button>' +
						'</td>' +
					'</tr>'
				);				
			});
			$('#attendance_hrd .table tbody').html(table_html.join(''));
			
			ATTENCANDE_HRD_OFFSET = Result.offset;
		}
	);
};

function attendance_hrd_decision(Handle, Value){
	if(confirm('Keputusan: ' + (Value ? 'Diterima' : 'Ditolak') + '?')){
		API(
			'/attendance/correction_hrd', 
			{ 
				'handle': Handle,
				'decision': Value
			}, 
			function(Result){
				attendance_hrd_record(ATTENCANDE_HRD_OFFSET);
			}
		);
	}
};