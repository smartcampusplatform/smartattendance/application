function API(EndPoint, Data, Callback){
	$.ajax({
		'type': 'POST',
		'contentType': false,
		'contentType': 'application/json',
		'dataType': 'json',
		
		'url': AAA_API + EndPoint,
		'data': JSON.stringify(Data),
		'headers': { 
			'Content-Type': 'application/json',
			'token': TOKEN 
		},
		
		'success': function(content, status, xhr){
			if(content.hasOwnProperty('message') && (content.message != '')){
				console.warn('API(' + EndPoint + '): ' + content.message);
			}

			if(content.hasOwnProperty('result') && (typeof(Callback) == 'function')){
				Callback(content.result);
			}
		},
		'error': function(xhr, status, error){
			console.error('API', EndPoint, Data, xhr, status, error);
		}
	});
};