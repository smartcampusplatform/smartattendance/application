var ATTENCANDE_OFFSET = 0;

function attendance_record(Offset = -1){
	API(
		'/attendance/record', 
		{ 'offset': (Offset != -1 ? Offset : ATTENCANDE_OFFSET) }, 
		function(Result){		
			var 
				pagenation_html = [],
				table_html = [],
				pointer = 0;
			
			for(pointer = 0; pointer <= Result.limit; pointer++){
				pagenation_html.push(
					'<li class="page-item">' +
						'<a class="page-link" href="#" onClick="attendance_record(' + pointer + ')">' +
						(pointer+1).toString() +
						'</a>' +
					'</li>'
				);
			}
			
			$('#attendance .pagination').html(pagenation_html.join(''));
			
			pointer = (Result.offset * RECORD_LIMIT) + 1;
			Result.record.forEach(function(data){
				table_html.push(
					'<tr>' +
						'<td>' + (pointer++) + '</td>' +
						'<td>' + data.onduty + '</td>' +
						'<td>' + data.offduty + '</td>' +
					'</tr>'
				);				
			});
			$('#attendance .table tbody').html(table_html.join(''));
			
			ATTENCANDE_OFFSET = Result.offset;
		}
	);
};