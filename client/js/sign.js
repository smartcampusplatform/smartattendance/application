var
	SIGN_NICKNAME = '',
	SIGN_PRIVILEGE = 0;

function SignAuth(Result){
	if(
		(Result == null) || 
		((Result.hasOwnProperty('nickname') ? Result.nickname : '') == '')
	){
		if($('#sign').length == 0){
			$('body').html(Base64Decode(LAYOUT.sign));
		}
	}else{
		if($('#dashboard').length == 0){
			SIGN_NICKNAME = (Result.hasOwnProperty('nickname') ? Result.nickname : '');
			SIGN_PRIVILEGE = (Result.hasOwnProperty('privilege') ? Result.privilege : 0);
			
			$('body').html(Base64Decode(LAYOUT.dashboard));
			$('#sign_nickname').html(SIGN_NICKNAME);
			ShowNavigation(SIGN_PRIVILEGE);
		}
	}
};

function SignIn(){
	API(
		'/sign/in', 
		{
			'username': $('#username').val(),
			'password': $('#password').val()
		}, 
		function(Result){
			SignAuth(Result)
		}
	);
};

function SignOut(){
	API('/sign/out', {}, function(Result){
		$('body').html(Base64Decode(LAYOUT.sign));
	});
};