var ATTENCANDE_CORRECTION_OFFSET = 0;

function attendance_correction_record(Offset = -1){
	API(
		'/attendance/record', 
		{ 'offset': (Offset != -1 ? Offset : ATTENCANDE_CORRECTION_OFFSET) }, 
		function(Result){		
			var 
				pagenation_html = [],
				table_html = [],
				pointer = 0;
			
			for(pointer = 0; pointer <= Result.limit; pointer++){
				pagenation_html.push(
					'<li class="page-item">' +
						'<a class="page-link" href="#" onClick="attendance_correction_record(' + pointer + ')">' +
						(pointer+1).toString() +
						'</a>' +
					'</li>'
				);
			}
			
			$('#attendance_correction .pagination').html(pagenation_html.join(''));
			
			pointer = (Result.offset * RECORD_LIMIT) + 1;
			Result.record.forEach(function(data){
				table_html.push(
					'<tr>' +
						'<td>' + (pointer++) + '</td>' +
						'<td>' + data.onduty + '</td>' +
						'<td>' + data.offduty + '</td>' +
						'<td><button type="button" class="btn btn-primary" onClick="attendance_correction_open(' + data.handle + ')">Koreksi Kehadiran</button></td>' +
					'</tr>'
				);				
			});
			$('#attendance_correction .table tbody').html(table_html.join(''));
			
			ATTENCANDE_CORRECTION_OFFSET = Result.offset;
		}
	);
};

function attendance_correction_open(Handle){
	API(
		'/attendance/get', 
		{ 'handle': Handle }, 
		function(Result){
			console.log(Result);
			$('#attendance_correction_handle').val(Result.handle);
			$('#attendance_correction_onduty').val(Result.onduty);
			$('#attendance_correction_offduty').val(Result.offduty);
			$('#attendance_correction_message').val('');
			$('#attendance_correction .modal').modal('show');
		}
	);
};

function attendance_correction_request(){
	API(
		'/attendance/correction_request', 
		{ 
			'attendance': parseInt($('#attendance_correction_handle').val()), 
			'onduty': $('#attendance_correction_onduty').val(), 
			'offduty': $('#attendance_correction_offduty').val(), 
			'message': $('#attendance_correction_message').val(), 
		}, 
		function(Result){
			if(Result){
				$('#attendance_correction .modal').modal('hide');				
			}else{
				alert('gagalll');
			}
		}
	);
};