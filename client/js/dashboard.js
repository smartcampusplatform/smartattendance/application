function ShowNavigation(Privilege){
	var 
		content = [],
		navigation = [
			{
				'caption': 'kehadiran', 
				'privilege': 0,
				'content': 'attendance', // panggil html
				'command': 'attendance_record' // panggil fungsi js
			},
			{
				'caption': 'koreksi kehadiran', 
				'privilege': 0,
				'content': 'attendance_correction', // panggil html
				'command': 'attendance_correction_record' // panggil fungsi js
			},
			{
				'caption': 'persetujuan kehadiran (SuperVisior)', 
				'privilege': PRIVILEGE_SUPERVISIOR,
				'content': 'attendance_supervisior', // panggil html
				'command': 'attendance_supervisior_record' // panggil fungsi js
			},{
				'caption': 'persetujuan kehadiran (HRD)', 
				'privilege': PRIVILEGE_HRD,
				'content': 'attendance_hrd', // panggil html
				'command': 'attendance_hrd_record' // panggil fungsi js
			},
			{
				'caption': 'poin kehadiran', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			{
				'caption': 'cuti', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			{
				'caption': 'kegiatan', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			
			{
				'caption': 'laporan', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			{
				'caption': 'persetujuan', 
				'privilege': PRIVILEGE_HRD | PRIVILEGE_SUPERVISIOR,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			
			{
				'caption': 'master kepegawaian', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
			{
				'caption': 'qr scanner', 
				'privilege': 0,
				'content': '', // panggil html
				'command': '' // panggil fungsi js
			},
		];
	
	navigation.forEach(function(data){
		if(
			(data.privilege == 0) ||
			((data.privilege & Privilege) != 0)
		){			
			content.push(
				'<li class="nav-item">' +
					// '<a class="nav-link active" href="#">' +
					'<a class="nav-link" href="#" onClick="ShowContent(\'' + data.content + '\', \'' + data.command + '\');">' + data.caption + '</a>' +
				'</li>'
			);
		}
	});
	
	$('#navigation').html(content.join(''));
};

function ShowContent(Name, Command){
	if(LAYOUT.hasOwnProperty(Name)){
		$('#main').html(Base64Decode(LAYOUT[Name]));
		
		if(typeof(window[Command]) == 'function'){
			window[Command]();
		}
	}else{
		alert(Name + ': Content not exists');
	}
};

