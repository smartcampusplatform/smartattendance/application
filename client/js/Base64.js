BASE64_TABLE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

if(typeof(window['Base64Decode']) == 'undefined'){
	window['Base64Decode'] = function(CipherText){
		var
			ret = '', index = 0,
			chr1, chr2, chr3,
			enc1, enc2, enc3, enc4;

		while(index < CipherText.length){
			enc1 = BASE64_TABLE.indexOf(CipherText.charAt(index++));
			enc2 = BASE64_TABLE.indexOf(CipherText.charAt(index++));
			enc3 = BASE64_TABLE.indexOf(CipherText.charAt(index++));
			enc4 = BASE64_TABLE.indexOf(CipherText.charAt(index++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			ret = ret + String.fromCharCode(chr1);

			if(enc3 != 64){
				ret += String.fromCharCode(chr2);
			}

			if(enc4 != 64){
				ret += String.fromCharCode(chr3);
			}
		}

		return ret;
	};
};