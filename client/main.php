<?php
	session_start();
	include("include.php");
	
	echo(
		"<!DOCTYPE html>" .
		"<html lang=\"en\">" .
			"<head>" .
				"<meta charset=\"utf-8\" />" .
				"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />" .
				"<title>Smart Attendance</title>" .
				implode("", array_map(function($data){
					return "<link href=\"" . $data . "\" rel=\"stylesheet\" />";
				}, GetCSS())) .
				implode("", array_map(function($data){
					return "<script src=\"" . $data . "\" type=\"text/javascript\"></script>";
				}, GetJS())) .
				"<script type=\"text/javascript\">" .
					"var " .
						"AAA_API='" . AAA_API . "'," .
						"TOKEN='" . GetToken() . "'," .
						"LAYOUT=" . json_encode(GetLayout()) . ";" .
					
					"$(document).ready(function(){" .
						"API('/sign/info', {}, function(Result){" .
							"SignAuth(Result);" .
						"});" .
					"});" .
				"</script>" .
			"</head>" .
			"<body></body>" .
		"</html>"

	);
	
?>